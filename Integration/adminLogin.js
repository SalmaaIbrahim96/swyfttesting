describe("Knowledge Base Application", () => {
  beforeEach(() => {
    cy.fixture("admin").as("admin");
  })

    it('Does not do much', function() {
        //expect(true).to.equal(false)
        cy.visit('http://35.233.12.50/login')

        cy
      .get('input[name="email"]')
      .type(this.admin.email)
      .should("have.value", 'superadmin@swyft.com');

      cy
      .get('input[name="password"]')
      .type(this.admin.password)
      .should("have.value", '#DHS6+jmu');

      cy.get("form").submit();

      cy.location("pathname").should("eq", "/");

    })


})