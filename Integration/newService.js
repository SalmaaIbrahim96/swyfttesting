
import 'cypress-file-upload';

describe("Knowledge Base Application", () => {
    beforeEach(() => {
      cy.fixture("service").as("service");
    })
  
      it('Does not do much', function() {
          //expect(true).to.equal(false)
          cy.visit('http://35.233.12.50/login')
  
          cy
        .get('input[name="email"]')
        .type(this.service.email)
        .should("have.value", 'superadmin@swyft.com');
  
        cy
        .get('input[name="password"]')
        .type(this.service.password)
        .should("have.value", '#DHS6+jmu');
  
        cy.get("form").submit();
  
        cy.location("pathname").should("eq", "/");

        cy.contains('Services').click();
        cy.location("pathname").should("eq", "/services");

        cy.contains('Add New Service').click();
        cy.location("pathname").should("eq", "/services/create");

        cy
        .get('input[name="ordering"]')
        .type(this.service.ordering)
        .should("have.value", 'Online');

        cy
        .get('input[name="english_name"]')
        .type(this.service.englishName)
        .should("have.value", 'Fast Shipping');

        cy
        .get('input[name="arabic_name"]')
        .type(this.service.arabicName)
        .should("have.value", 'توصيل سريع');

        const fileName = 'logo.png';
 
        cy.fixture(fileName).then(fileContent => {
        cy.get('input[name="image"]').upload(
            { fileContent, fileName, mimeType: 'logo/png' },
        );
        });

        cy
        .get('select[name="status"]')
        .select('Inactive').should('have.value', '0')

        cy.get("form").submit();







  
      })
  
  
  })